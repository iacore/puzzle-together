#[cfg(test)]
pub use assert2::{check, let_assert};
pub use puzzletogether_core::prelude::*;

pub use nom::{
    branch::alt,
    bytes::complete::*,
    character::complete::anychar,
    combinator::{all_consuming, cut, map, map_res, opt, recognize},
    error::{make_error, ErrorKind},
    multi::*,
    sequence::{delimited, pair, preceded, terminated, tuple},
    Err::*,
    IResult,
};

pub(crate) use crate::color::*;
pub(crate) use crate::object::*;
pub(crate) use crate::rule::*;
pub(crate) use crate::tokens::*;

pub use parse_hyperlinks::take_until_unbalanced;

pub use nom::error::ParseError as NomParseError;
pub use nom::error::Error as NomError;
pub type IErr<T> = nom::Err<NomError<T>>;

pub(crate) fn iconvert<E, F>(e: nom::Err<F>) -> nom::Err<E>
where
    E: From<F>,
{
    nom::Err::convert(e)
}

pub fn lift<I, T, E>(
    mut f: impl FnMut(I) -> IResult<I, T>,
) -> impl FnMut(I) -> Result<(I, T), nom::Err<E>>
where
    E: From<NomError<I>>,
{
    move |input| f(input).map_err(iconvert)
}

#[derive(Debug, from_enum::From)]
pub enum ParseError<'a> {
    InvalidCopySprite(#[from] crate::object::InvalidCopySprite),
    InvalidObjectDefinition((Option<&'a str>, ObjectSpriteUnlinked)),
    InvalidObjectInLegend(&'a str),
    // rule is invalid in syntax
    ParseRule(#[from] ParseRuleError),
    // rule is invalid in logic
    Rule(#[from] SanitizeRuleError),
    // error when parsing byte stream
    Nom(#[from] NomError<&'a str>),
}

const GARBAGE_TEXT: &str = "TODO: placeholder to satisfy lifetime checker";

impl<'a> ParseError<'a> {
    pub fn squeeze(self) -> NomError<&'a str> {
        match self {
            ParseError::Nom(inner) => inner,
            other => {
                log::error!("Error squeezed: {other:?}");
                make_error("", ErrorKind::Verify)
            }
        }
    }
    pub fn forget_clone(&self) -> ParseError<'static> {
        use ParseError::*;
        match self {
            InvalidCopySprite(x) => InvalidCopySprite(x.clone()),
            InvalidObjectDefinition((_todo, b)) => {
                InvalidObjectDefinition((Some(GARBAGE_TEXT), b.clone()))
            }
            InvalidObjectInLegend(_todo) => InvalidObjectInLegend(GARBAGE_TEXT),
            Rule(x) => Rule(x.clone()),
            ParseRule(x) => ParseRule(forget_clone(x)),
            Nom(x_todo) => Nom(NomError {
                input: GARBAGE_TEXT,
                code: x_todo.code,
            }),
        }
    }
}

fn forget_clone<T: Clone>(x: &NomError<T>) -> NomError<T> {
    NomError {
        input: x.input.clone(),
        code: x.code,
    }
}

// impl<'a, 'b, 'c> From<NomError<&'a [&'b str]>> for ParseError<'c> {
//     fn from(x: NomError<&'a [&'b str]>) -> Self {
//         ParseError::ParseRule({
//             NomError {
//                 input: x.input.iter().map(|x| (*x).to_owned()).collect(),
//                 code: x.code,
//             }
//         })
//     }
// }

pub type ParseResult<'a, T, I = &'a str> = IResult<I, T, ParseError<'a>>;

impl<'a> NomParseError<&'a str> for ParseError<'a> {
    fn from_error_kind(input: &'a str, kind: ErrorKind) -> Self {
        Self::Nom(make_error(input, kind))
    }

    fn append(_input: &str, _kind: ErrorKind, other: Self) -> Self {
        other
    }
}

impl<'a, 'b, 'c> NomParseError<&'a [&'b str]> for ParseError<'c> {
    fn from_error_kind(input: &'a [&'b str], kind: ErrorKind) -> Self {
        Self::ParseRule(make_error(
            input.iter().map(|x| (*x).to_owned()).collect(),
            kind,
        ))
    }

    fn append(_input: &[&str], _kind: ErrorKind, other: Self) -> Self {
        other
    }
}

impl<'a, 'b: 'a, 'c: 'a, 'd, E> nom::error::FromExternalError<&'b [&'c str], E> for ParseError<'d>
where
    Self: From<E>,
{
    fn from_external_error(input: &'b [&'c str], kind: ErrorKind, _e: E) -> Self {
        Self::from_error_kind(input, kind)
    }
}
