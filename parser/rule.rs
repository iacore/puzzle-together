use crate::prelude::*;

pub type ParseRuleError = NomError<Vec<String>>;

/// parse rule from tokens
///
/// e.g.
/// [ >  Player | Crate ] -> [  >  Player | > Crate  ]
fn parse_rule<'b: 'd, 'c, 'd>(
    input: &[&'b str],
    objects: &'c [ObjectDefinition],
) -> Result<Rule, nom::Err<ParseError<'d>>> {
    let rule_side = parse_rule_side(objects);
    let rule_side2 = parse_rule_side(objects);

    let keyword = alt((
        map(token("late"), |_| RuleMod::Late),
        map(token("horizontal"), |_| RuleMod::Horizontal),
        map(token("vertical"), |_| RuleMod::Vertical),
    ));
    let prefix_modifiers = many0(keyword);

    let mut rule = all_consuming(tuple((
        prefix_modifiers,
        rule_side,
        token("->"),
        rule_side2,
    )));

    match rule(input) {
        Ok((_, (modifiers, lhs, _, rhs))) => Ok(Rule {
            modifiers,
            left: lhs,
            right: rhs,
        }),
        Err(e) => Err(e.map(|x| x.forget_clone())),
    }
}

fn parse_rule_side<'a, 'b, 'c, 'd, 'f>(
    objects: &'d [ObjectDefinition],
) -> impl (FnMut(&'a [&'b str]) -> IResult<&'a [&'b str], Vec<RulePart>, ParseError<'c>>) + 'd
where
    'a: 'b,
    'b: 'a,
    'c: 'a,
    'a: 'c,
    'a: 'd,
{
    let token_identifier = token_match(is_identifier);
    let movement = alt((
        map(token(">"), |_| Movement::Forward),
        map(token("<"), |_| Movement::Backward),
        map(token("v"), |_| Movement::Right),
        map(token("^"), |_| Movement::Left),
    ));
    let object = alt((
        map(token("..."), |_| RuleObjectSym::Ellipsis),
        map_res(
            opt(pair(opt(movement), token_identifier)),
            |x| -> Result<_, ParseError> {
                if let Some((movement, ident)) = x {
                    let index = find_object_by_name_simple(objects, ident)?;
                    Ok(if let Some(movement) = movement {
                        RuleObjectSym::PreMove(movement, index)
                    } else {
                        RuleObjectSym::Plain(index)
                    })
                } else {
                    Ok(RuleObjectSym::None)
                }
            },
        ),
    ));
    let bracketed = map(
        delimited(token("["), separated_list1(token("|"), object), token("]")),
        RulePart::LinedUp,
    );
    
    many1(bracketed)
}

pub(crate) fn rule<'a>(
    input: &'a str,
    objects: &[ObjectDefinition],
) -> ParseResult<'a, Rule> {
    let (input, tokens) = tokens(input).map_err(iconvert)?;
    let parsed_rule = parse_rule(&tokens, objects)?;
    Ok((input, parsed_rule))
}

#[test]
fn test_rules() {
    for example_rule in [
        "[ > Player | Crate ] -> [ > Player | > Crate ]",
        "[ < Player | Crate ] -> [ < Player | < Crate ]",
        "late [ Crate | Crate | Crate ] -> [ | | ]",
        "[ Eyeball | ... | Player ] -> [ > Eyeball | ... | Player ]",
        "late [ Sprite | ... | Player ] -> [ Player | ... | Sprite ]",
        "late [ Sprite | ... | Player ] -> [ Temp | ... | Sprite ]",
        "late [ Temp ] -> [ Player ]",
        "[ > Kitty | ... | Fruit ] -> [ | ... | Kitty ]",
        "[ > Kitty ] -> [ Kitty ]",
        "Horizontal [ > Player | Block ] -> [ > Player | > Block ]",
        "[ > Player ] [ Sumo ] -> [ > Player ] [ > Sumo ]",
    ] {
        println!("{example_rule}");
        let objects = vec![
            ObjectDefinition::dummy("Player"),
            ObjectDefinition::dummy("Crate"),
            ObjectDefinition::dummy("Eyeball"),
            ObjectDefinition::dummy("Sprite"),
            ObjectDefinition::dummy("Temp"),
            ObjectDefinition::dummy("Fruit"),
            ObjectDefinition::dummy("Kitty"),
            ObjectDefinition::dummy("Block"),
            ObjectDefinition::dummy("Sumo"),
        ];
        check!(let Ok(("", _)) = rule(example_rule, &objects));
    }
}

// todo: add unhappy tests