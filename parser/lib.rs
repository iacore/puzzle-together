#![feature(iterator_try_collect)]

pub mod color;
pub mod directive;
pub mod object;
pub mod rule;
pub mod tokens;

mod prelude;

use crate::prelude::*;

use std::collections::BTreeMap;

pub fn root(input: &str) -> ParseResult<PuzzleScript> {
    use directive::directives;
    let (input, directives) = directives(input)?;
    let (input, objects) = section_objects(input)?;
    let (input, legend) = section_legend(input, &objects)?;
    let (input, sounds) = section_sounds(input, &objects)?;
    let (input, collision_layers) = section_collisionlayers(input, &objects)?;
    let (input, rules) = section_rules(input, &objects)?;
    let (input, win_conditions) = section_winconditions(input)?;
    let (input, levels) = section_levels(input)?;

    Ok((
        input,
        PuzzleScript {
            directives,
            objects,
            legend,
            sounds,
            collision_layers,
            rules,
            win_conditions,
            levels,
        },
    ))
}

fn section_objects(input: &str) -> ParseResult<Vec<ObjectDefinition>> {
    let (input, objects) = preceded(
        lift(tuple((tokens::garbo, tokens::line_lowercase_exact("objects")))),
        fold_many0(
            preceded(lift(tokens::garbo), object::object_def),
            || Ok(Vec::new()),
            |acc, item| -> Result<Vec<ObjectDefinition>, object::InvalidCopySprite> {
                let mut acc = acc?;
                object::link_object(&mut acc, item)?;
                Ok(acc)
            },
        ),
    )(input)?;
    let objects = objects.map_err(|e| Failure(e.into()))?;
    Ok((input, objects))
}

fn section_legend<'a>(
    input: &'a str,
    objects: &[ObjectDefinition],
) -> ParseResult<'a, BTreeMap<char, Vec<ObjectType>>> {
    let (input, legend_rules) = preceded(
        tuple((
            tokens::garbo,
            tokens::line_lowercase_exact("legend"),
            tokens::garbo,
        )),
        separated_list0(
            tag("\n"),
            map(
                tuple((
                    whitespace0,
                    anychar,
                    whitespace0,
                    tag("="),
                    whitespace0,
                    separated_list1(tuple((whitespace1, tag("and"), whitespace1)), identifier),
                    whitespace0,
                )),
                |(_, c, _, _, _, list, _)| (c, list),
            ),
        ),
    )(input)
    .map_err(iconvert)?;
    let mut legend = BTreeMap::new();
    for (c, names) in legend_rules {
        let names: Vec<ObjectType> = names
            .iter()
            .map(|name| find_object_by_name(objects, name))
            .try_collect()?;
        legend.insert(c, names);
    }
    Ok((input, legend))
}

fn section_sounds<'a>(
    input: &'a str,
    _objects: &[ObjectDefinition],
) -> ParseResult<'a, Vec<Sound>> {
    let (input, _) = preceded(
        tuple((tokens::garbo, tokens::line_lowercase_exact("sounds"))),
        // todo
        take(0usize),
    )(input)
    .map_err(iconvert)?;
    Ok((input, vec![]))
}

fn section_collisionlayers<'a>(
    input: &'a str,
    objects: &[ObjectDefinition],
) -> ParseResult<'a, Vec<Vec<ObjectType>>> {
    let (input, layers) = preceded(
        tuple((
            tokens::garbo,
            tokens::line_lowercase_exact("collisionlayers"),
            tokens::garbo,
        )),
        // todo
        many0(lineof(separated_list1(
            tuple((whitespace0, tag(","), whitespace0)),
            identifier,
        ))),
    )(input)
    .map_err(iconvert)?;

    let sanitized_layers = layers
        .into_iter()
        .map(|layer| {
            layer
                .into_iter()
                .map(|x| find_object_by_name(objects, x))
                .try_collect()
        })
        .try_collect()?;
    Ok((input, sanitized_layers))
}

fn section_rules<'a>(input: &'a str, objects: &[ObjectDefinition]) -> ParseResult<'a, Vec<Rule>> {
    let (input, rules): (_, Vec<Rule>) = preceded(
        lift(tuple((tokens::garbo, tokens::line_lowercase_exact("rules")))),
        many0(preceded(lift(garbo), lineof(|i| rule(i, objects)))),
    )(input)?;
    for rule in &rules {
        rule.sanitize().map_err(|e| Failure(e.into()))?;
    }
    Ok((input, vec![]))
}

fn section_winconditions(_input: &str) -> ParseResult<Vec<Condition>> {
    todo!()
    // let (input, _) = preceded(
    //     tuple((tokens::garbo, tokens::line_lowercase_exact("winconditions"))),
    //     // todo
    //     take(0usize),
    // )(input)
    // .map_err(iconvert)?;
    // Ok((input, vec![]))
}

fn section_levels(_input: &str) -> ParseResult<Vec<LevelOrSection>> {
    todo!()
    // let (input, _) = preceded(
    //     tuple((tokens::garbo, tokens::line_lowercase_exact("levels"))),
    //     // todo
    //     take(0usize),
    // )(input)
    // .map_err(iconvert)?;
    // Ok((input, vec![]))
}

#[test]
fn parse_whole() {
    let input = include_str!("examples/sokoban.puzzlescript");
    check!(let Ok(_) = root(input));
}
