use crate::prelude::*;

use parse_display::Display;

pub(crate) fn object_def(input: &str) -> ParseResult<ObjectDefinitionUnlinked> {
    use ObjectSpriteUnlinked::*;

    let (input, ((ident, maybe_copy), spritedef)) = pair(
        lineof(tuple((
            identifier,
            map(opt(tuple((whitespace1, tag("copy:"), identifier))), |m| {
                m.map(|(_, _, x)| x)
            }),
        ))),
        alt((
            map(lineof(color), SingleColor),
            map(
                tuple((
                    lineof(separated_list0(whitespace1, color)),
                    opt(chargrid(hex1)),
                )),
                |(colormap, maybe_sprite)| {
                    if let Some(sprite) = maybe_sprite {
                        Detailed { colormap, sprite }
                    } else {
                        DetailedCopy {
                            colormap,
                            copyof: "".into(),
                        }
                    }
                },
            ),
        )),
    )(input)
    .map_err(iconvert)?;
    let spritedef = match (maybe_copy, spritedef) {
        (None, x @ SingleColor(_)) => x,
        (None, x @ Detailed { .. }) => x,
        (Some(name), DetailedCopy { colormap, .. }) => DetailedCopy {
            colormap,
            copyof: name.into(),
        },
        other => return Err(Error(ParseError::InvalidObjectDefinition(other))),
    };
    Ok((
        input,
        ObjectDefinitionUnlinked {
            sprite: spritedef,
            name: ident.into(),
        },
    ))
}

#[test]
fn parse_objectdef() {
    let prompt = r#"
Background 
LIGHTGRAY GRAY
00010
11111
01000
11111
00010

"#;
    check!(let Ok(_) = preceded(lift(garbo), object_def)(prompt));
}

#[derive(Debug, Display, Clone)]
pub enum InvalidCopySprite {
    #[display("Invalid object sprite specification: nonexistent object to copy from: `{}`")]
    InvalidTarget(String),
    #[display("target object doesn't have detailed sprite: `{}`")]
    CopyTargetNotDetailed(String),
}

pub(crate) fn link_object(
    acc: &mut Vec<ObjectDefinition>,
    item: ObjectDefinitionUnlinked,
) -> Result<(), InvalidCopySprite> {
    let value = sanitize_object(item, acc)?;
    acc.push(value);
    Ok(())
}

fn sanitize_object(
    item: ObjectDefinitionUnlinked,
    acc: &mut [ObjectDefinition],
) -> Result<ObjectDefinition, InvalidCopySprite> {
    use ObjectSpriteUnlinked::*;
    let ObjectDefinitionUnlinked { name, sprite } = item;
    let sprite = match sprite {
        SingleColor(c) => ObjectSprite::SingleColor(c),
        Detailed { colormap, sprite } => ObjectSprite::Detailed {
            colormap,
            sprite: sprite.into(),
        },
        DetailedCopy { colormap, copyof } => {
            let Some(target) = acc.iter().find(|n| n.name == copyof) else { return Err(InvalidCopySprite::InvalidTarget(copyof)) };
            let ObjectSprite::Detailed { sprite, .. } = &target.sprite else { return Err(InvalidCopySprite::CopyTargetNotDetailed(copyof)) };
            ObjectSprite::Detailed {
                colormap,
                sprite: sprite.clone(),
            }
        }
    };
    Ok(ObjectDefinition { name, sprite })
}

pub(crate) fn find_object_by_name<'a, 'b, 'c: 'b>(
    objects: &'a [ObjectDefinition],
    name: &'c str,
) -> Result<ObjectType, nom::Err<ParseError<'b>>> {
    find_object_by_name_simple(objects, name).map_err(Failure)
}

pub(crate) fn find_object_by_name_simple<'a, 'b, 'c: 'b>(
    objects: &'a [ObjectDefinition],
    name: &'c str,
) -> Result<ObjectType, ParseError<'b>> {
    let lower = name.to_ascii_lowercase();
    let Some((i, _)) = objects.iter().enumerate().find(|(_, o)| o.name.to_ascii_lowercase() == lower) else {
        return Err(
            ParseError::InvalidObjectInLegend(name)
        )
    };
    Ok(ObjectType(i))
}
