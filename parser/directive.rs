use crate::prelude::*;

pub fn directives(input: &str) -> ParseResult<Vec<Directive>> {
    let (input, items) = many0(map(crate::tokens::directive_with_garbo, |(k, v)| {
        Directive::new(k, v.unwrap_or(""))
    }))(input).map_err(iconvert)?;
    Ok((input, items))
}

#[test]
fn parse_directive_multi() {
    let prompt = r#"(
Hello there! 

This is a special starting template for PuzzleScript Plus highlighting the most important differences from vanilla PuzzleScript. It's intended as a "Hello World" of sorts. So it's handy if you already know how vanilla PuzzleScript works before diving into this.

If you immediately want to working on a game instead, consider using the "Basic" or "Blank Project" example instead.

For more info, don't forget to check out the NEW EXAMPLES and the DOCS. Enjoy!
)

title My PS+ Game
author My Name Here
homepage auroriax.github.io/PuzzleScript

tween_length 0.03
level_select
runtime_metadata_twiddling
text_controls Your PuzzleScript Plus journey starts here! \nHello world!

"#;
    use Directive::*;
    let parsed = directives(prompt);
    let_assert!(Ok(("\n\n", ds)) = parsed);

    assert_eq!(ds[0], Title("My PS+ Game".into()));
    assert_eq!(ds[1], Author("My Name Here".into()));
    assert_eq!(ds[2], Website("auroriax.github.io/PuzzleScript".into()));
    assert_eq!(
        ds[5],
        Unrecognized {
            key: "runtime_metadata_twiddling".into(),
            value: "".into()
        }
    );
}
