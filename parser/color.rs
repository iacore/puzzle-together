use crate::prelude::*;

fn from_hex(input: &str) -> Result<u8, std::num::ParseIntError> {
    u8::from_str_radix(input, 16)
}

pub(crate) fn is_hex_digit(c: char) -> bool {
    c.is_ascii_hexdigit()
}

fn hex2(input: &str) -> IResult<&str, u8> {
    map_res(take_while_m_n(2, 2, is_hex_digit), from_hex)(input)
}

pub(crate) fn hex1(input: &str) -> IResult<&str, u8> {
    map(map_res(take_while_m_n(1, 1, is_hex_digit), from_hex), |x| {
        x * 0x11
    })(input)
}

/// #rrggbbaa
fn color_rrggbbaa(input: &str) -> IResult<&str, FixedColor> {
    let (input, _) = tag("#")(input)?;
    let (input, (red, green, blue, alpha)) = tuple((hex2, hex2, hex2, hex2))(input)?;
    Ok((input, FixedColor::rgba(red, green, blue, alpha)))
}

/// #rrggbb
fn color_rrggbb(input: &str) -> IResult<&str, FixedColor> {
    let (input, _) = tag("#")(input)?;
    let (input, (red, green, blue)) = tuple((hex2, hex2, hex2))(input)?;
    Ok((input, FixedColor::rgb(red, green, blue)))
}

/// #rgba
fn color_rgba(input: &str) -> IResult<&str, FixedColor> {
    let (input, _) = tag("#")(input)?;
    let (input, (red, green, blue, alpha)) = tuple((hex1, hex1, hex1, hex1))(input)?;
    Ok((input, FixedColor::rgba(red, green, blue, alpha)))
}

/// #rgb
fn color_rgb(input: &str) -> IResult<&str, FixedColor> {
    let (input, _) = tag("#")(input)?;
    let (input, (red, green, blue)) = tuple((hex1, hex1, hex1))(input)?;
    Ok((input, FixedColor::rgb(red, green, blue)))
}

fn color_named_one(input: &str, c: NamedColor) -> IResult<&str, NamedColor> {
    let display: &'static str = c.into();
    let (input, _) = tag_no_case(display)(input)?;
    Ok((input, c))
}

fn color_named(input: &str) -> IResult<&str, NamedColor> {
    for color in NamedColor::iter() {
        if let Ok(pair) = color_named_one(input, color) {
            return Ok(pair)
        }
    }
    Err(Error(make_error(input, ErrorKind::OneOf)))
}

pub(crate) fn color(input: &str) -> IResult<&str, Color> {
    let parser_fixed = map(
        alt((color_rrggbbaa, color_rrggbb, color_rgba, color_rgb)),
        Color::Fixed,
    );
    let parser_named = map(color_named, Color::Named);
    alt((parser_fixed, parser_named))(input)
}

#[test]
fn parse_color() {
    assert_eq!(
        color("#2F14DF"),
        Ok((
            "",
            Color::Fixed(FixedColor {
                r: 0x2F,
                g: 0x14,
                b: 0xDF,
                a: 0xFF,
            })
        ))
    );
    assert_eq!(
        color("#2f14df"),
        Ok((
            "",
            Color::Fixed(FixedColor {
                r: 0x2F,
                g: 0x14,
                b: 0xDF,
                a: 0xFF,
            })
        ))
    );
    assert_eq!(
        color("#2F14DF23"),
        Ok((
            "",
            Color::Fixed(FixedColor {
                r: 0x2F,
                g: 0x14,
                b: 0xDF,
                a: 0x23,
            })
        ))
    );
    assert_eq!(
        color("#2F14"),
        Ok((
            "",
            Color::Fixed(FixedColor {
                r: 0x22,
                g: 0xFF,
                b: 0x11,
                a: 0x44,
            })
        ))
    );
    assert_eq!(
        color("#2F1"),
        Ok((
            "",
            Color::Fixed(FixedColor {
                r: 0x22,
                g: 0xFF,
                b: 0x11,
                a: 0xff,
            })
        ))
    );
    assert_eq!(
        color("LightBlue"),
        Ok(("", Color::Named(NamedColor::LightBlue)))
    );
}
