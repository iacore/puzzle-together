//! line parsers
use crate::prelude::*;

fn comment_block(input: &str) -> IResult<&str, ()> {
    let parser = delimited(tag("("), take_until_unbalanced('(', ')'), tag(")"));
    map(parser, |_| ())(input)
}

fn comment_line(input: &str) -> IResult<&str, ()> {
    map(
        tuple((
            tag("="),
            take_while(|c| c != '\n'),
            take_while_m_n(0, 1, |c| c == '\n'),
        )),
        |_| (),
    )(input)
}

fn comment(input: &str) -> IResult<&str, ()> {
    alt((comment_line, comment_block))(input)
}

fn linebreak(input: &str) -> IResult<&str, ()> {
    let parser = tag("\n");
    map(parser, |_| ())(input)
}

fn is_identifier_start(c: char) -> bool {
    c == '_' || c.is_alphabetic()
}
fn is_identifier_rest(c: char) -> bool {
    is_identifier_start(c) || c.is_numeric()
}

pub(crate) fn identifier(input: &str) -> IResult<&str, &str> {
    recognize(pair(
        take_while_m_n(1, 1, is_identifier_start),
        take_while(is_identifier_rest),
    ))(input)
}

/// non-empty line
fn line(input: &str) -> IResult<&str, &str> {
    take_while1(|c| c != '\n' && c != '=' && c != '(')(input)
}

fn line_not_keyword(input_0: &str) -> IResult<&str, &str> {
    let (input, line) = line(input_0)?;
    if is_keyword(line) {
        return Err(IErr::Error(make_error(input_0, ErrorKind::Verify)));
    }
    Ok((input, line))
}

pub(crate) fn lineof<'a, O, E, F>(f: F) -> impl FnMut(&'a str) -> IResult<&str, O, E>
where
    E: NomParseError<&'a str>,
    F: nom::Parser<&'a str, O, E>,
{
    terminated(f, pair(whitespace0, tag("\n")))
}

fn is_inline_white(c: char) -> bool {
    c != '\n' && c.is_whitespace()
}

pub(crate) fn whitespace0<'a, E>(input: &'a str) -> IResult<&str, &str, E>
where
    E: NomParseError<&'a str>,
{
    take_while(is_inline_white)(input)
}

pub(crate) fn whitespace1<'a, E>(input: &'a str) -> IResult<&str, &str, E>
where
    E: NomParseError<&'a str>,
{
    take_while1(is_inline_white)(input)
}

// pub(crate) fn whitespace(input: &str) -> IResult<&str, &str> {
//     take_while1(|c: char| c != '\n' && c.is_whitespace())(input)
// }

/// the latter half of directive; maybe empty
/// `key[ value]`
fn directive_value(input: &str) -> Result<&str, IErr<&str>> {
    let (input, _) = whitespace1(input)?;
    Ok(input)
}

/// input to this should be sanitized with `line()`
fn directive(input: &str) -> Result<(&str, Option<&str>), IErr<&str>> {
    let (input, key) = identifier(input)?;

    // try parse the value part
    let value = match directive_value(input) {
        Ok(value) => Some(value),
        Err(Failure(e)) => return Err(Failure(e)),
        Err(_) => None,
    };

    Ok((key, value))
}

pub(crate) const DIRECTIVE_KEYWORDS: &[&str] = &[
    "objects",
    "legend",
    "sounds",
    "collisionlayers",
    "rules",
    "winconditions",
    "levels",
];

fn is_keyword(directive_key: &str) -> bool {
    let lower = directive_key.trim().to_ascii_lowercase();
    DIRECTIVE_KEYWORDS.iter().any(|s| *s == lower)
}

pub(crate) fn garbo(input: &str) -> IResult<&str, ()> {
    fold_many0(alt((comment, linebreak)), || (), |x, _| x)(input)
}

/// parse one directive, discarding leading garbage (like comments)
pub(crate) fn directive_with_garbo(input: &str) -> IResult<&str, (&str, Option<&str>)> {
    map_res(tuple((garbo, line_not_keyword)), |(_, s)| directive(s))(input)
}

pub(crate) fn line_lowercase_exact(expected: &'static str) -> impl Fn(&str) -> IResult<&str, &str> {
    move |input: &str| -> IResult<&str, &str> {
        map_res(line, |parsed| {
            if parsed.to_ascii_lowercase() == expected {
                Ok(parsed)
            } else {
                Err(IErr::Error(make_error(input, ErrorKind::Verify)))
            }
        })(input)
    }
}

#[test]
fn parse_comment() {
    let testcase = r#"(
        some comment here
        ( nested brackets )
    )"#;
    check!(let Ok(("", _)) = comment(testcase));

    let testcase = r#"(
        some comment here
        ( not paired
    )"#;
    check!(let Err(_) = comment(testcase));
}

#[test]
fn parse_directive() {
    assert_eq!(directive("abc def"), Ok(("abc", Some("def"))));
    assert_eq!(directive("abc"), Ok(("abc", None)));
    assert_eq!(directive("abc   "), Ok(("abc", Some(""))));
    assert_eq!(directive("abc "), Ok(("abc", Some(""))));
}

pub(crate) fn chargrid<'a, O, E, F>(f: F) -> impl FnMut(&'a str) -> IResult<&str, Grid<O>, E>
where
    E: NomParseError<&'a str>,
    F: nom::Parser<&'a str, O, E> + Copy,
    O: Default,
{
    move |input| {
        let (input, list) = separated_list1(tag("\n"), many1(f))(input)?;
        let mut grid = Grid::new(0, list[0].len());
        for row in list {
            grid.push_row(row); // todo: check row size is correct
        }
        Ok((input, grid))
    }
    // terminated(f, tag("\n"))
}

pub(crate) fn tokens(input: &str) -> IResult<&str, Vec<&str>> {
    delimited(
        whitespace0,
        separated_list1(whitespace1, take_while1(|c| !is_inline_white(c))),
        whitespace0,
    )(input)
}

fn lowercase_byte(c: u8) -> u8 {
    match c {
        b'A'..=b'Z' => c - b'A' + b'a',
        _ => c,
    }
}

pub(crate) fn eq_lowercase(lhs: &str, rhs: &str) -> bool {
    let (lhs, rhs) = (lhs.bytes(), rhs.bytes());
    lhs.len() == rhs.len()
        && lhs
            .zip(rhs)
            .all(|(a, b)| lowercase_byte(a) == lowercase_byte(b))
}

pub(crate) fn token_match<'a, 'c, 'd: 'c, Error: NomParseError<&'c [&'d str]>>(
    f: impl Fn(&str) -> bool + 'a,
) -> impl Fn(&'c [&'d str]) -> IResult<&'c [&'d str], &'d str, Error> + 'a {
    move |i0| {
        let mut i = i0.iter();
        let Some(first_token) = i.next() else {
            return Err(Error(make_error(i0, ErrorKind::TakeWhileMN)));
        };
        if f(first_token) {
            Ok((i.as_slice(), first_token))
        } else {
            Err(Error(make_error(i0, ErrorKind::Tag)))
        }
    }
}

/// case insensitive token
pub(crate) fn token<'a, 'c, 'd: 'c, Error: NomParseError<&'c [&'d str]>>(
    t: &'a str,
) -> impl Fn(&'c [&'d str]) -> IResult<&'c [&'d str], &'d str, Error> + 'a {
    token_match(|t2| eq_lowercase(t, t2))
}

pub(crate) fn is_identifier(s: &str) -> bool {
    !s.is_empty() && {
        let mut s = s.chars();
        is_identifier_start(s.next().expect("s.len() > 0")) && s.all(is_identifier_rest)
    }
}
