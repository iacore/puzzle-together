#[derive(Debug, Clone, PartialEq)]
pub struct Rule {
    pub modifiers: Vec<RuleMod>,
    pub left: Vec<RulePart>,
    pub right: Vec<RulePart>,
}

impl Rule {
    pub fn sanitize(&self) -> Result<(), SanitizeRuleError> {
        todo!()
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct ObjectType(pub usize);

#[derive(Debug, Clone, PartialEq)]
pub enum Movement {
    Forward,
    Backward,
    Left,
    Right,
}

#[derive(Debug, Clone, PartialEq)]
pub enum RuleMod {
    Horizontal,
    Vertical,
    Late,
}

/// What inside a square bracket
/// 
/// [ bla | > bla | bla ]
#[derive(Debug, Clone, PartialEq)]
pub enum RulePart {
    /// objects close together in a line
    LinedUp(Vec<RuleObjectSym>),
}

/// symbolic object in rule part
#[derive(Debug, Clone, PartialEq)]
pub enum RuleObjectSym {
    /// represent no object is here
    ///
    /// used to add/remove objects
    None,
    /// represent an object type, nothing special
    Plain(ObjectType),
    /// movement marker
    /// without `late`: This object want to move
    /// with `late`: This object just moved
    PreMove(Movement, ObjectType),
    /// `...`
    /// any number of tiles in between
    Ellipsis,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Condition {
    AllOn(ObjectType, ObjectType),
}

#[derive(Debug, Clone)]
pub enum SanitizeRuleError {}
