use strum::{EnumCount, EnumIter, IntoStaticStr, IntoEnumIterator};

#[derive(IntoStaticStr, EnumCount, EnumIter, Clone, Copy, PartialEq, Eq, Debug)]
pub enum NamedColor {
    Black,
    White,
    Grey,
    DarkGrey,
    LightGrey,
    Gray,
    DarkGray,
    LightGray,
    Red,
    DarkRed,
    LightRed,
    Brown,
    DarkBrown,
    LightBrown,
    Orange,
    Yellow,
    Green,
    DarkGreen,
    LightGreen,
    Blue,
    LightBlue,
    DarkBlue,
    Purple,
    Pink,
}

#[test]
fn named_color_count_eq_24() {
    assert_eq!(NamedColor::COUNT, 24);
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct FixedColor {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl FixedColor {
    pub const fn rgb(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b, a: 0xff }
    }
    pub const fn rgba(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }
}

const fn rgb(rgb: u32) -> FixedColor {
    FixedColor::rgb(
        (rgb >> 16) as u8,
        (rgb >> 8) as u8,
        rgb as u8,
    )
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Color {
    Fixed(FixedColor),
    Named(NamedColor),
}

pub struct Palette {
    colors: [FixedColor; 24],
}

impl Palette {
    pub const fn new(colors: [FixedColor; 24]) -> Self {
        Self { colors }
    }
    pub fn named_to_fixed(&self, c: NamedColor) -> FixedColor {
        let i = NamedColor::iter()
            .enumerate()
            .find(|(_, c_)| *c_ == c)
            .expect("known enum variant")
            .0;
        self.colors[i]
    }
}

const BUILTIN_PALETTE_COUNT: usize = 14;

#[rustfmt::skip]
pub const BUILTIN_PALETTES: [Palette; BUILTIN_PALETTE_COUNT] = [
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x555555), rgb(0x555500), rgb(0xaaaaaa), rgb(0x555555), rgb(0x555500), rgb(0xaaaaaa),
    rgb(0xff0000), rgb(0xaa0000), rgb(0xff5555), rgb(0xaa5500), rgb(0x550000), rgb(0xffaa00), rgb(0xff5500), rgb(0xffff55),
    rgb(0x55aa00), rgb(0x005500), rgb(0xaaff00), rgb(0x5555aa), rgb(0xaaffff), rgb(0x000055), rgb(0x550055), rgb(0xffaaff), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x7f7f7c), rgb(0x3e3e44), rgb(0xbaa7a7), rgb(0x7f7f7c), rgb(0x3e3e44), rgb(0xbaa7a7),
    rgb(0xa7120c), rgb(0x880606), rgb(0xba381f), rgb(0x57381f), rgb(0x3e2519), rgb(0x8e634b), rgb(0xba4b32), rgb(0xc0ba6f),
    rgb(0x517525), rgb(0x385d12), rgb(0x6f8e44), rgb(0x5d6fa7), rgb(0x8ea7a7), rgb(0x4b575d), rgb(0x3e3e44), rgb(0xba381f), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0xbbbbbb), rgb(0x333333), rgb(0xffeedd), rgb(0xbbbbbb), rgb(0x333333), rgb(0xffeedd),
    rgb(0xdd1111), rgb(0x990000), rgb(0xff4422), rgb(0x663311), rgb(0x331100), rgb(0xaa6644), rgb(0xff6644), rgb(0xffdd66),
    rgb(0x448811), rgb(0x335500), rgb(0x88bb77), rgb(0x8899dd), rgb(0xbbddee), rgb(0x666688), rgb(0x665555), rgb(0x997788), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x9d9d9d), rgb(0x697175), rgb(0xcccccc), rgb(0x9d9d9d), rgb(0x697175), rgb(0xcccccc),
    rgb(0xbe2633), rgb(0x732930), rgb(0xe06f8b), rgb(0xa46422), rgb(0x493c2b), rgb(0xeeb62f), rgb(0xeb8931), rgb(0xf7e26b),
    rgb(0x44891a), rgb(0x2f484e), rgb(0xa3ce27), rgb(0x1d57f7), rgb(0xb2dcef), rgb(0x1b2632), rgb(0x342a97), rgb(0xde65e2), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x7c7c7c), rgb(0x080808), rgb(0xbcbcbc), rgb(0x7c7c7c), rgb(0x080808), rgb(0xbcbcbc),
    rgb(0xf83800), rgb(0x881400), rgb(0xf87858), rgb(0xac7c00), rgb(0x503000), rgb(0xfce0a8), rgb(0xfca044), rgb(0xf8b800),
    rgb(0x00b800), rgb(0x005800), rgb(0xb8f8b8), rgb(0x0058f8), rgb(0x3cbcfc), rgb(0x0000bc), rgb(0x6644fc), rgb(0xf878f8), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x909090), rgb(0x404040), rgb(0xb0b0b0), rgb(0x909090), rgb(0x404040), rgb(0xb0b0b0),
    rgb(0xa03c50), rgb(0x700014), rgb(0xdc849c), rgb(0x805020), rgb(0x703400), rgb(0xcb9870), rgb(0xccac70), rgb(0xecd09c),
    rgb(0x58b06c), rgb(0x006414), rgb(0x70c484), rgb(0x1c3c88), rgb(0x6888c8), rgb(0x000088), rgb(0x3c0080), rgb(0xb484dc), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x3e3e3e), rgb(0x313131), rgb(0x9cbcbc), rgb(0x3e3e3e), rgb(0x313131), rgb(0x9cbcbc),
    rgb(0xf56ca2), rgb(0xa63577), rgb(0xffa9cf), rgb(0xb58c53), rgb(0x787562), rgb(0xb58c53), rgb(0xeb792d), rgb(0xffe15f),
    rgb(0x00ff4f), rgb(0x2b732c), rgb(0x97c04f), rgb(0x0f88d3), rgb(0x00fffe), rgb(0x293a7b), rgb(0xff6554), rgb(0xeb792d), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x555555), rgb(0x555555), rgb(0xaaaaaa), rgb(0x555555), rgb(0x555555), rgb(0xaaaaaa),
    rgb(0xff5555), rgb(0xaa0000), rgb(0xff55ff), rgb(0xaa5500), rgb(0xaa5500), rgb(0xffff55), rgb(0xff5555), rgb(0xffff55),
    rgb(0x00aa00), rgb(0x00aaaa), rgb(0x55ff55), rgb(0x5555ff), rgb(0x55ffff), rgb(0x0000aa), rgb(0xaa00aa), rgb(0xff55ff), ]),
Palette::new([
    rgb(0x3d2d2e), rgb(0xddf1fc), rgb(0x9fb2d4), rgb(0x7b8272), rgb(0xa4bfda), rgb(0x9fb2d4), rgb(0x7b8272), rgb(0xa4bfda),
    rgb(0x9d5443), rgb(0x8c5b4a), rgb(0x94614c), rgb(0x89a78d), rgb(0x829e88), rgb(0xaaae97), rgb(0xd1ba86), rgb(0xd6cda2),
    rgb(0x75ac8d), rgb(0x8fa67f), rgb(0x8eb682), rgb(0x88a3ce), rgb(0xa5adb0), rgb(0x5c6b8c), rgb(0xd39fac), rgb(0xc8ac9e), ]),
Palette::new([
    rgb(0x010912), rgb(0xfdeeec), rgb(0x051d40), rgb(0x091842), rgb(0x062151), rgb(0x051d40), rgb(0x091842), rgb(0x062151),
    rgb(0xad4576), rgb(0x934765), rgb(0xab6290), rgb(0x61646b), rgb(0x3d2d2d), rgb(0x8393a0), rgb(0x0a2227), rgb(0x0a2541),
    rgb(0x75ac8d), rgb(0x0a2434), rgb(0x061f2e), rgb(0x0b2c79), rgb(0x809ccb), rgb(0x08153b), rgb(0x666a87), rgb(0x754b4d), ]),
Palette::new([
    rgb(0x6f686f), rgb(0xd1b1e2), rgb(0xb9aac1), rgb(0x8e8b84), rgb(0xc7b5cd), rgb(0xb9aac1), rgb(0x8e8b84), rgb(0xc7b5cd),
    rgb(0xa11f4f), rgb(0x934765), rgb(0xc998ad), rgb(0x89867d), rgb(0x797f75), rgb(0xab9997), rgb(0xce8c5c), rgb(0xf0d959),
    rgb(0x75bc54), rgb(0x599d79), rgb(0x90cf5c), rgb(0x8fd0ec), rgb(0xbcdce7), rgb(0x0b2c70), rgb(0x9b377f), rgb(0xcd88e5), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x7f7f7f), rgb(0x636363), rgb(0xafafaf), rgb(0x7f7f7f), rgb(0x636363), rgb(0xafafaf),
    rgb(0xff0000), rgb(0x7f0000), rgb(0xff7f7f), rgb(0xff7f00), rgb(0x7f7f00), rgb(0xffff00), rgb(0xff007f), rgb(0xffff7f),
    rgb(0x01ff00), rgb(0x007f00), rgb(0x7fff7f), rgb(0x0000ff), rgb(0x7f7fff), rgb(0x00007f), rgb(0x7f007f), rgb(0xff7fff), ]),
Palette::new([
    rgb(0x000000), rgb(0xffffff), rgb(0x6c6c6c), rgb(0x444444), rgb(0x959595), rgb(0x6c6c6c), rgb(0x444444), rgb(0x959595),
    rgb(0x68372b), rgb(0x3f1e17), rgb(0x9a6759), rgb(0x433900), rgb(0x221c02), rgb(0x6d5c0d), rgb(0x6f4f25), rgb(0xb8c76f),
    rgb(0x588d43), rgb(0x345129), rgb(0x9ad284), rgb(0x6c5eb5), rgb(0x70a4b2), rgb(0x352879), rgb(0x6f3d86), rgb(0xb044ac), ]),
Palette::new([
    rgb(0x202527), rgb(0xeff8fd), rgb(0x7b7680), rgb(0x3c3b44), rgb(0xbed0d7), rgb(0x7b7680), rgb(0x3c3b44), rgb(0xbed0d7),
    rgb(0xbd194b), rgb(0x6b1334), rgb(0xef2358), rgb(0xb52e1c), rgb(0x681c12), rgb(0xe87b45), rgb(0xff8c10), rgb(0xfbd524),
    rgb(0x36bc3c), rgb(0x317610), rgb(0x8ce062), rgb(0x3f62c6), rgb(0x57bbe0), rgb(0x2c2fa0), rgb(0x7037d9), rgb(0xec2b8f), ]),
];

pub const BUILTIN_PALETTE_NAMES: [&str; BUILTIN_PALETTE_COUNT] = [
    "mastersystem",
    "gameboycolour",
    "amiga",
    "arnecolors",
    "famicom",
    "atari",
    "pastel",
    "ega",
    "amstrad",
    "proteus_mellow",
    "proteus_rich",
    "proteus_night",
    "c64",
    "whitingjp",
];

pub const DEFAULT_PALETTE: &str = "arnecolors";

pub fn get_palette(name: &str) -> Option<&'static Palette> {
    let Some((i, _)) = BUILTIN_PALETTE_NAMES.iter().enumerate().find(|(_, name_)| **name_ == name) else { return None };
    Some(&BUILTIN_PALETTES[i])
}
