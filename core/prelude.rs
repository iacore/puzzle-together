use std::collections::BTreeMap;
use std::rc::Rc;

pub use crate::color::{Color, FixedColor, NamedColor};
pub use crate::rule::*;
pub use grid::Grid;
pub use strum::IntoEnumIterator;

#[derive(Debug, Clone, Default, PartialEq)]
pub struct PuzzleScript {
    pub directives: Vec<Directive>,
    pub objects: Vec<ObjectDefinition>,
    /// char => objects[index]
    pub legend: BTreeMap<char, Vec<ObjectType>>,
    pub sounds: Vec<Sound>,
    pub collision_layers: Vec<Vec<ObjectType>>,
    pub rules: Vec<Rule>,
    pub win_conditions: Vec<Condition>,
    pub levels: Vec<LevelOrSection>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Directive {
    /// game title
    Title(String),
    Author(String),
    Website(String),
    /// help message, usually showing game controls in menu screen
    HelpMessage(String),

    /// level select is always on
    LevelSelectOn,

    Unrecognized {
        key: String,
        value: String,
    },
}

impl Directive {
    /// Parse unstrutured directive into named ones
    ///
    /// used in parser
    pub fn new(key: impl Into<String>, value: impl Into<String>) -> Self {
        let (key, value) = (key.into(), value.into());
        use Directive::*;
        match key.to_ascii_lowercase().as_str() {
            "title" => Title(value),
            "author" => Author(value),
            "homepage" => Website(value),
            "text_controls" => HelpMessage(value),

            "level_select" => LevelSelectOn,

            _ => Unrecognized { key, value },
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct ObjectDefinition {
    pub name: String,
    pub sprite: ObjectSprite,
}

impl ObjectDefinition {
    pub fn dummy(name: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            sprite: ObjectSprite::SingleColor(Color::Named(NamedColor::Black)),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum ObjectSprite {
    SingleColor(Color),
    Detailed {
        colormap: Vec<Color>,
        sprite: Rc<Grid<u8>>,
    },
}

#[derive(Debug, Clone, PartialEq)]
pub struct ObjectDefinitionUnlinked {
    pub name: String,
    pub sprite: ObjectSpriteUnlinked,
}

#[derive(Debug, Clone, PartialEq)]
pub enum ObjectSpriteUnlinked {
    SingleColor(Color),
    Detailed {
        colormap: Vec<Color>,
        sprite: Grid<u8>,
    },
    DetailedCopy {
        colormap: Vec<Color>,
        copyof: String,
    },
}

#[derive(Debug, Clone, PartialEq)]
pub struct Sound {
    // todo
}

#[derive(Debug, Clone, PartialEq)]
pub enum LevelOrSection {
    Section(String),
    Level(Level),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Level(Grid<char>);
